header {* Useful definitions for reasoning about protocols *}
theory Protocols
imports
  Main
  Communication
begin

type_synonym protocol = "history \<Rightarrow> time \<Rightarrow> event set \<Rightarrow> bool"

text {* Has a particular agent satisfied a particular protocol at a particular time?
        *}
definition satisfied_protocol_at :: "agent \<Rightarrow> protocol \<Rightarrow> history \<Rightarrow> time \<Rightarrow> bool"
  where "satisfied_protocol_at a p h t = p h t {e. (e, t) \<in> h \<and> causal_agent e = a}"

text {* If a particular agent has always satisfied a particular protocol in the
        past, is it always possible for them to continue to satisfy it now? *}
definition continuously_satisfiable :: "agent \<Rightarrow> protocol \<Rightarrow> bool" where
  "continuously_satisfiable a p \<longleftrightarrow>
    (\<forall> h t. possible_history h \<and> (\<forall> u < t. satisfied_protocol_at a p h u)
      \<longrightarrow> (\<exists> es.  (\<forall> e \<in> es. doable a (events_before h t) (events_at h t) e)
                \<and> p h t es)
    )"

text {* Does a particular protocol allow or disallow different things based on
        information an agent doesn't possess?  The idea behind this definition is
        that if a set of events is permissible in one history, and another history
        is indistinguishable from the first by the relevant agent, then the
        equivalent set of events should also be permissible. *}
definition non_clairvoyant :: "agent \<Rightarrow> protocol \<Rightarrow> bool" where
  "non_clairvoyant A p \<longleftrightarrow>
    (\<forall> h h' f t es. possible_history h
                  \<and> possible_history h'
                  \<and> indistinguishability_map A (messages_received_before A h t) f
                  \<and> image (apfst (lift_message_map_event f))
                          (relevant_subhistory A t h)
                      = relevant_subhistory A t h'
                  \<and> p h t es
      \<longrightarrow> p h' t (image (lift_message_map_event f) es)
    )"

subsection {* Meet (conjunction) of protocols *}
definition protocol_Meet :: "protocol set \<Rightarrow> protocol" where
  "protocol_Meet ps h t es = (\<forall> p \<in> ps. p h t es)"

theorem Meet_non_clairvoyant:
  assumes each_non_clairvoyant: "\<forall> p \<in> ps. non_clairvoyant A p"
  shows "non_clairvoyant A (protocol_Meet ps)"
proof -
  { fix h h' f t es
    assume  hypotheses: "possible_history h
                  \<and> possible_history h'
                  \<and> indistinguishability_map A (messages_received_before A h t) f
                  \<and> image (apfst (lift_message_map_event f))
                          (relevant_subhistory A t h)
                      = relevant_subhistory A t h'"
      and   Meetsatisfied: "protocol_Meet ps h t es"
    have
      "protocol_Meet ps h' t (image (lift_message_map_event f) es)"
    proof -
      { fix p
        assume "p \<in> ps"
        with Meetsatisfied
          have satisfied: "p h t es" by (unfold protocol_Meet_def) auto
        from each_non_clairvoyant and `p \<in> ps`
          have "non_clairvoyant A p" ..
        with hypotheses and satisfied
          have satisfied':
                "p h' t (image (lift_message_map_event f) es)"
            by (unfold non_clairvoyant_def) auto
      }
      thus
        "protocol_Meet ps h' t (image (lift_message_map_event f) es)"
          by (unfold protocol_Meet_def) auto
    qed
  }
  thus "non_clairvoyant A (protocol_Meet ps)" by (unfold non_clairvoyant_def) auto
qed

subsection {* Timely delivery protocol *}
text {* Does the communication layer get messages from one specific actor to
        another specific actor within a specified time limit? *}
type_synonym duration = time
definition timely_delivery :: "actor \<Rightarrow> actor \<Rightarrow> duration \<Rightarrow> protocol" where
  "timely_delivery a b d h t es \<longleftrightarrow>
    (\<forall> m s.  s + d = t
          \<and> (Send a b m, s) \<in> h
          \<and> \<not>(\<exists> r. s < r \<and> r < t \<and> (Receive b m, r) \<in> h)
      \<longrightarrow> Receive b m \<in> es
    )"

theorem timely_delivery_continuously_satisfiable:
  assumes "d > 0"
  shows "continuously_satisfiable CommunicationLayer (timely_delivery a b d)"
proof -
  { fix h t
    let ?es = "{Receive b m | a b m u. u + d = t \<and> (Send a b m, u) \<in> h}"
    have "( \<forall> e \<in> ?es
              . doable CommunicationLayer (events_before h t) (events_at h t) e
          )
          \<and> timely_delivery a b d h t ?es"
    proof default+
      fix e
      assume "e \<in> ?es"
      then obtain a b m u
        where "e = Receive b m"
          and "u + d = t"
          and "(Send a b m, u) \<in> h"
            by auto
      from `d > 0` and `u + d = t` and `(Send a b m, u) \<in> h`
      have "Send a b m \<in> events_before h t"
        by (unfold events_before_def) (auto simp add: exI [of _ "t - d"])
      thus "doable CommunicationLayer (events_before h t) (events_at h t) e"
        by (unfold `e = Receive b m`) (rule communicationLayer_transmit_doable)
    next
      show "timely_delivery a b d h t ?es" by (unfold timely_delivery_def) auto
    qed
  }
  thus "continuously_satisfiable CommunicationLayer (timely_delivery a b d)"
    by (unfold continuously_satisfiable_def) blast
qed

theorem timely_delivery_non_clairvoyant:
  assumes "d > 0"
  shows "non_clairvoyant CommunicationLayer (timely_delivery a b d)"
proof -
  { fix h h' f t es
    assume  indistinguishable:
              "image (apfst (lift_message_map_event f))
                  (relevant_subhistory CommunicationLayer t h)
                = relevant_subhistory CommunicationLayer t h'"
      and   timely: "timely_delivery a b d h t es"
    have "timely_delivery a b d h' t (image (lift_message_map_event f) es)"
    proof -
      { fix s m'
        assume  timeup: "s + d = t"
          and   sent': "(Send a b m', s) \<in> h'"
          and   notreceived': "\<not>(\<exists> r. s < r \<and> r < t \<and> (Receive b m', r) \<in> h')"
        from timeup and `d > 0` have "s < t" by simp
        with sent'
          have "(Send a b m', s) \<in> relevant_subhistory CommunicationLayer t h'"
            by (unfold relevant_subhistory_def)
                (simp add: send_affects_communication)
        with indistinguishable
          obtain x
            where apfstx: "(Send a b m', s) = apfst (lift_message_map_event f) x"
              and xin: "x \<in> relevant_subhistory CommunicationLayer t h"
            by blast
        from apfstx have sndx: "snd x = s" by (rule apfst_convE) simp
        from apfstx
          have "Send a b m' = fst (apfst (lift_message_map_event f) x)"
            by (metis fst_conv)
        then obtain m
          where fstx: "fst x = Send a b m" and mapm: "f m = m'"
            by (cases "fst x") auto
        from fstx and sndx and xin have sent: "(Send a b m, s) \<in> h"
          by (unfold relevant_subhistory_def) auto

        have notreceived: "\<not>(\<exists> r. s < r \<and> r < t \<and> (Receive b m, r) \<in> h)"
        proof
          assume "\<exists> r. s < r \<and> r < t \<and> (Receive b m, r) \<in> h"
          then obtain r
            where sr: "s < r"
              and rt: "r < t"
              and received: "(Receive b m, r) \<in> h"
                by auto
          from rt and received
            have "(Receive b m, r) \<in> relevant_subhistory CommunicationLayer t h"
              by (unfold relevant_subhistory_def)
                  (simp add: receive_affects_communication)
          with indistinguishable
            have "apfst (lift_message_map_event f) (Receive b m, r)
                    \<in> relevant_subhistory CommunicationLayer t h'"
              by blast
          with mapm
            have "(Receive b m', r) \<in> h'"
              by (unfold relevant_subhistory_def) simp
          with sr and rt and notreceived' show False by simp
        qed
        with timely and timeup and sent have "Receive b m \<in> es"
          by (unfold timely_delivery_def) auto
        with mapm
          have "Receive b m' \<in> image (lift_message_map_event f) es" by force
      }
      thus "timely_delivery a b d h' t (image (lift_message_map_event f) es)"
        by (unfold timely_delivery_def) auto
    qed
  }
  thus "non_clairvoyant CommunicationLayer (timely_delivery a b d)"
    by (unfold non_clairvoyant_def) auto
qed

subsection {* Self-encryption protocol *}
text {* This protocol requires an actor who is constructing a new encrypted message
        to include themselves in the pair of actors who can decrypt it. *}
definition self_encryption :: "actor \<Rightarrow> protocol" where
  "self_encryption a h t es = (
    \<forall> b c d m M.  a \<noteq> c
                \<and> a \<noteq> d
                \<and> learnable a M (Encrypted c d m)
                \<and> Send a b M \<in> es
      \<longrightarrow> (\<exists> M'. learnable a M' (Encrypted c d m)
                \<and> Receive a M' \<in> events_before h t
          )
  )"

theorem self_encryption_non_clairvoyant:
  "non_clairvoyant (Actor a) (self_encryption a)"
proof -
  { fix h h' f t es
    assume  im: "indistinguishability_map
                  (Actor a) (messages_received_before (Actor a) h t) f"
      and   fsubhistory: "image (apfst (lift_message_map_event f))
                                (relevant_subhistory (Actor a) t h)
                            = relevant_subhistory (Actor a) t h'"
      and   satisfied: "self_encryption a h t es"
    from im have bij: "bij f" unfolding indistinguishability_map_def ..
    have "self_encryption a h' t (image (lift_message_map_event f) es)"
    proof -
      { fix b c' d' n N
        assume  ac': "a \<noteq> c'"
          and   ad': "a \<noteq> d'"
          and   learnable': "learnable a N (Encrypted c' d' n)"
          and   sendable': "Send a b N \<in> image (lift_message_map_event f) es"
        from sendable' and sendable'_implies_sendable obtain M
          where mapM: "f M = N" and sendable: "Send a b M \<in> es"
            by blast
        from bij and mapM have mapN: "inv f N = M" by simp
        from im and learnable'
          have "learnable a (inv f N) (inv f (Encrypted c' d' n))"
            by (rule learnable_inverse_indistinguishability_map)
        hence "learnable a M (inv f (Encrypted c' d' n))"
          by (subst mapN [symmetric])
        moreover
          from im
            have "indistinguishable
                    (Actor a)
                    (messages_received_before (Actor a) h t)
                    (inv f (Encrypted c' d' n))
                    (f (inv f (Encrypted c' d' n)))"
              by (unfold indistinguishability_map_def) blast
          with bij
            have "type_of_message (inv f (Encrypted c' d' n)) = EncryptedType"
              by (simp add: indistinguishable_message_type)
          then obtain c and d and m
              where invfenccdn: "inv f (Encrypted c' d' n) = Encrypted c d m"
            by (induction rule: message.induct) auto
        ultimately have learnable: "learnable a M (Encrypted c d m)" by simp
        from bij and invfenccdn
          have fenccdm: "f (Encrypted c d m) = Encrypted c' d' n"
            by simp
        with im and ac' and ad' have "a \<noteq> c" and "a \<noteq> d"
          by (unfold indistinguishability_map_def) auto
        with satisfied and learnable and sendable
          obtain M' where learnableM': "learnable a M' (Encrypted c d m)"
                      and received: "Receive a M' \<in> events_before h t"
            by (unfold self_encryption_def) blast
        let ?N' = "f M'"
        from    learnableM'
            and fenccdm
            and im
            and learnable_indistinguishability_map
          have learnableN': "learnable a ?N' (Encrypted c' d' n)" by metis
        from received
          obtain t' where tt': "t' < t" and "(Receive a M', t') \<in> h"
            by (unfold events_before_def) auto
        hence "(Receive a M', t') \<in> relevant_subhistory (Actor a) t h"
          by (unfold relevant_subhistory_def) (simp add: receiver_affected)
        hence "apfst (lift_message_map_event f) (Receive a M', t')
              \<in> image (apfst (lift_message_map_event f))
                      (relevant_subhistory (Actor a) t h)"
          by (rule imageI)
        with fsubhistory and tt' have "Receive a ?N' \<in> events_before h' t"
          by (unfold events_before_def relevant_subhistory_def) auto
        with learnableN'
          have "\<exists> N'. learnable a N' (Encrypted c' d' n)
                    \<and> Receive a N' \<in> events_before h' t"
            by auto
      }
      thus "self_encryption a h' t (image (lift_message_map_event f) es)"
        by (unfold self_encryption_def) blast
    qed
  }
  thus "non_clairvoyant (Actor a) (self_encryption a)"
    by (unfold non_clairvoyant_def) auto
qed

end
