header {* Responsible Trust Traversal Protocol *}
theory RTTP
imports
  Main
  Communication
  Protocols
begin

inductive signatures_for_complete_agreement
  :: "nat \<Rightarrow> message list \<Rightarrow> message list \<Rightarrow> message list \<Rightarrow> bool" for i
  where "signatures_for_complete_agreement i [] [m] []"
      | "signatures_for_complete_agreement i as (m' # ms) sigs
        \<Longrightarrow> signatures_for_complete_agreement
                i
                ((ActorName a) # as)
                (m # m' # ms)
                ((Signature a (MessageList [Number i, m, m'])) # sigs)"

fun complete_agreement :: "message \<Rightarrow> bool" where
  "complete_agreement
    ( MessageList
      [ Number i
      , MessageList as
      , MessageList ps
      , MessageList sigs
      ]
    )
    \<longleftrightarrow> distinct as
      \<and> list_all (\<lambda> p. type_of_message p = EncryptedType) ps
      \<and> signatures_for_complete_agreement i as (last ps # ps) sigs"
  | "complete_agreement _ = False"

definition rttp_promises
  :: "actor
  \<Rightarrow> (actor \<Rightarrow> duration)
  \<Rightarrow> (actor \<Rightarrow> payment \<Rightarrow> actor \<Rightarrow> payment \<Rightarrow> bool)
  \<Rightarrow> protocol"
  where "rttp_promises a d w h t es
    \<longleftrightarrow> ( \<forall> i b a' m a'' c m'
              . Construct a 
                  ( Signature a
                    ( MessageList
                      [ Number i
                      , Encrypted b a' m
                      , Encrypted a'' c m'
                      ]
                    )
                  )
                \<in> es
            \<longrightarrow> a' = a
              \<and> a'' = a
              \<and> ( \<exists> t x t' y
                    . m = MessageList [Number t, Number x]
                    \<and> m' = MessageList [Number t', Number y]
                    \<and> t' + d b < t
                    \<and> w b x c y
                )
              \<and> ( \<forall> b' M c' M'
                      . Construct a 
                          ( Signature a
                            ( MessageList
                              [ Number i
                              , Encrypted b' a M
                              , Encrypted a c' M'
                              ]
                            )
                          )
                        \<in> events_before h t \<union> es
                    \<longrightarrow> c' = c \<and> M' = m'
                )
        )"

end