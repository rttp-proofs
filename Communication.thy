header {* Communication layer *}
theory Communication
imports
  Main
  Miscellany
begin

subsection {* Messages *}
type_synonym actor = nat

datatype_new message
  = ActorName actor               -- {* specify an actor *}
  | Number nat                    -- {* arbitrary data, as a natural number *}
  | Signature actor message       -- {* an actor's signature for a message *}
  | Encrypted actor actor message -- {* a message encrypted for two actors *}
  | MessageList "message list"    -- {* a list of messages *}

datatype_new message_type
  = ActorNameType
  | NumberType
  | SignatureType
  | EncryptedType
  | MessageListType

fun type_of_message :: "message \<Rightarrow> message_type"
  where "type_of_message (ActorName _) = ActorNameType"
      | "type_of_message (Number _) = NumberType"
      | "type_of_message (Signature _ _) = SignatureType"
      | "type_of_message (Encrypted _ _ _) = EncryptedType"
      | "type_of_message (MessageList _) = MessageListType"

text {* What are the components of a message, from an omniscient point of view? *}
inductive contains_message :: "message \<Rightarrow> message \<Rightarrow> bool"
  where contains_message_self: "contains_message M M"
      | contains_message_encrypted:
          "contains_message M (Encrypted _ _ m) \<Longrightarrow> contains_message M m"
      | contains_message_list:
          "m \<in> set ms \<Longrightarrow> contains_message M (MessageList ms)
            \<Longrightarrow> contains_message M m"

text {* If a particular message has been constructed, what other messages must also
        have been constructed? *}
inductive constructed_from :: "message \<Rightarrow> message \<Rightarrow> bool" for M
  where constructed_from_self: "constructed_from M M"
      | constructed_from_signature:
          "constructed_from M (Signature _ m) \<Longrightarrow> constructed_from M m"
      | constructed_from_encrypted:
          "constructed_from M (Encrypted _ _ m) \<Longrightarrow> constructed_from M m"
      | constructed_from_list:
          "m \<in> set ms \<Longrightarrow> constructed_from M (MessageList ms)
          \<Longrightarrow> constructed_from M m"

lemma actor_name_constructed_from:
  "constructed_from (ActorName a) m \<Longrightarrow> m = ActorName a"
  by (induction rule: constructed_from.induct) simp_all

lemma number_constructed_from:
  "constructed_from (Number x) m \<Longrightarrow> m = Number x"
  by (induction rule: constructed_from.induct) simp_all

lemma signature_constructed_from:
  "constructed_from (Signature a M) m
  \<Longrightarrow> m = Signature a M \<or> constructed_from M m"
proof (induction rule: constructed_from.induct)
  show "Signature a M = Signature a M \<or> constructed_from M (Signature a M)" by simp
  fix b m
  assume "Signature b m = Signature a M \<or> constructed_from M (Signature b m)"
  hence "constructed_from M m"
  proof
    assume "Signature b m = Signature a M"
    hence "m = M" by simp
    thus "constructed_from M m" by (simp add: constructed_from_self)
  next
    assume "constructed_from M (Signature b m)"
    thus "constructed_from M m" by rule
  qed
  thus "m = Signature a M \<or> constructed_from M m" ..
next
  fix b c m
  assume "Encrypted b c m = Signature a M
          \<or> constructed_from M (Encrypted b c m)"
  with constructed_from_encrypted
    show "m = Signature a M \<or> constructed_from M m"
      by blast
next
  fix m ms
  assume  "m \<in> set ms"
    and   "MessageList ms = Signature a M
            \<or> constructed_from M (MessageList ms)"
  with constructed_from_list show "m = Signature a M \<or> constructed_from M m"
    by blast
qed

lemma encrypted_constructed_from:
  "constructed_from (Encrypted a b M) m
  \<Longrightarrow> m = Encrypted a b M \<or> constructed_from M m"
proof (induction rule: constructed_from.induct)
  case constructed_from_self
  show "Encrypted a b M = Encrypted a b M
        \<or> constructed_from M (Encrypted a b M)"
    by simp
next
  case (constructed_from_signature c m)
  from constructed_from_signature.IH have "constructed_from M (Signature c m)"
    by simp
  hence "constructed_from M m" by rule
  thus "m = Encrypted a b M \<or> constructed_from M m" ..
next
  case (constructed_from_encrypted c d m)
  from constructed_from_encrypted.IH have "constructed_from M m"
  proof
    assume "Encrypted c d m = Encrypted a b M"
    thus "constructed_from M m" by (simp add: constructed_from_self)
  next
    assume "constructed_from M (Encrypted c d m)"
    thus "constructed_from M m" by rule
  qed
  thus "m = Encrypted a b M \<or> constructed_from M m" ..
next
  case (constructed_from_list m ms)
  from constructed_from_list.IH have "constructed_from M (MessageList ms)"
    by simp
  with constructed_from_list.hyps(1) have "constructed_from M m" by rule
  thus "m = Encrypted a b M \<or> constructed_from M m" ..
qed

lemma list_constructed_from:
  "constructed_from (MessageList ms) m
  \<Longrightarrow> m = MessageList ms \<or> (\<exists> M \<in> set ms. constructed_from M m)"
proof (induction rule: constructed_from.induct)
  case constructed_from_self
  show "MessageList ms = MessageList ms
        \<or> (\<exists> M \<in> set ms. constructed_from M (MessageList ms))"
    by simp
next
  case (constructed_from_signature a m)
  from constructed_from_signature.IH
    have "\<exists> M \<in> set ms. constructed_from M (Signature a m)"
      by simp
  hence "\<exists> M \<in> set ms. constructed_from M m"
    by (metis constructed_from.constructed_from_signature)
  thus "m = MessageList ms \<or> (\<exists> M \<in> set ms. constructed_from M m)" ..
next
  case (constructed_from_encrypted a b m)
  from constructed_from_encrypted.IH
    have "\<exists> M \<in> set ms. constructed_from M (Encrypted a b m)"
      by simp
  hence "\<exists> M \<in> set ms. constructed_from M m"
    by (metis constructed_from.constructed_from_encrypted)
  thus "m = MessageList ms \<or> (\<exists> M \<in> set ms. constructed_from M m)" ..
next
  case (constructed_from_list m ms')
  from constructed_from_list.IH have "\<exists> M \<in> set ms. constructed_from M m"
  proof
    assume "MessageList ms' = MessageList ms"
    with `m \<in> set ms'` have "m \<in> set ms" by simp
    with constructed_from_self show "\<exists> M \<in> set ms. constructed_from M m"
      by blast
  next
    assume "\<exists> M \<in> set ms. constructed_from M (MessageList ms')"
    with `m \<in> set ms'` show "\<exists> M \<in> set ms. constructed_from M m"
      by (metis constructed_from.constructed_from_list)
  qed
  thus "m = MessageList ms \<or> (\<exists> M \<in> set ms. constructed_from M m)" ..
qed

lemma constructed_from_transitive:
  assumes "constructed_from M' M"
  shows "constructed_from M m \<Longrightarrow> constructed_from M' m"
proof (induction rule: constructed_from.induct)
  case constructed_from_self
  from assms show "constructed_from M' M" .
next
  case (constructed_from_signature a m)
  from constructed_from_signature.IH show "constructed_from M' m" by rule
next
  case (constructed_from_encrypted a b m)
  from constructed_from_encrypted.IH show "constructed_from M' m" by rule
next
  case (constructed_from_list m ms)
  from constructed_from_list.hyps(1) and constructed_from_list.IH
    show "constructed_from M' m" by rule
qed

text {* What can a particular actor learn from a particular message? *}
inductive learnable :: "actor \<Rightarrow> message \<Rightarrow> message \<Rightarrow> bool" for a M
  where identity_learnable: "learnable a M M"
          -- {* the message itself *}
      | encrypted1_learnable: "learnable a M (Encrypted a _ m) \<Longrightarrow> learnable a M m"
      | encrypted2_learnable: "learnable a M (Encrypted _ a m) \<Longrightarrow> learnable a M m"
          -- {* the body of a learnable message encrypted for the actor *}
      | listed_learnable:
          "m \<in> set l \<Longrightarrow> learnable a M (MessageList l) \<Longrightarrow> learnable a M m"
          -- {* any element of a learnable list of messages *}

lemma learnable_implies_constructed_from:
  "learnable a M m \<Longrightarrow> constructed_from M m"
proof (induction rule: learnable.induct)
  show "constructed_from M M" by rule
  fix b m
  { assume "constructed_from M (Encrypted a b m)"
    thus "constructed_from M m" by rule
  next
    assume "constructed_from M (Encrypted b a m)"
    thus "constructed_from M m" by rule
  }
next
  fix m ms
  assume "m \<in> set ms" and "constructed_from M (MessageList ms)"
  thus "constructed_from M m" by rule
qed

text {* What messages can a particular actor construct from a particular set of
        messages? *}
inductive constructible :: "actor \<Rightarrow> message set \<Rightarrow> message \<Rightarrow> bool" for a Ms
  where learnable_constructible:
          "M \<in> Ms \<Longrightarrow> learnable a M m \<Longrightarrow> constructible a Ms m"
          -- {* any message learnable from any message in the set *}
      | actorName_constructible: "constructible a Ms (ActorName _)"
          -- {* any actor's name *}
      | number_constructible: "constructible a Ms (Number _)"
          -- {* any number *}
      | signed_constructible:
          "constructible a Ms m \<Longrightarrow> constructible a Ms (Signature a m)"
          -- {* an otherwise constructible message, signed by this actor *}
      | encrypted_constructible:
          "constructible a Ms m \<Longrightarrow> constructible a Ms (Encrypted _ _ m)"
          -- {* an otherwise constructible message, encrypted to any pair of actors *}
      | listed_constructible:
          "(\<forall> m \<in> set l. constructible a Ms m)
            \<Longrightarrow> constructible a Ms (MessageList l)"
          -- {* a list of otherwise constructible messages *}

subsubsection {* Indistinguishability of messages *}
datatype_new agent = CommunicationLayer | Actor actor

text {* What pairs of messages should we not expect a particular agent to be able
        to distinguish between, given a particular set of messages to work from?
        This definition isn't intended to express a limit on what an attacker might
        be able to do; rather, it's meant to limit what we might reasonably expect
        an honest agent to be able to do without resorting to sophisticated
        cryptanalysis or anything similarly difficult. *}
inductive indistinguishable :: "agent \<Rightarrow> message set \<Rightarrow> message \<Rightarrow> message \<Rightarrow> bool"
  where "indistinguishable CommunicationLayer _ _ _"
      | "indistinguishable _ _ m m"
      | "\<not> constructible a Ms m \<Longrightarrow> \<not> constructible a Ms m'
          \<Longrightarrow> indistinguishable (Actor a) Ms (Signature _ m) (Signature _ m')"
      | "indistinguishable A Ms m m'
          \<Longrightarrow> indistinguishable A Ms (Signature a m) (Signature a m')"
      | "A \<noteq> Actor a \<Longrightarrow> A \<noteq> Actor b \<Longrightarrow> A \<noteq> Actor c \<Longrightarrow> A \<noteq> Actor d
          \<Longrightarrow> indistinguishable A _ (Encrypted a b _) (Encrypted c d _)"
      | "indistinguishable A Ms m m'
          \<Longrightarrow> indistinguishable A Ms (Encrypted a b m) (Encrypted a b m')"
      | "indistinguishable A Ms m m'
          \<Longrightarrow> indistinguishable A Ms (MessageList ms) (MessageList ms')
          \<Longrightarrow> indistinguishable A Ms
                (MessageList (m # ms))
                (MessageList (m' # ms'))"

lemma indistinguishable_symmetric:
  "indistinguishable A Ms m m' \<Longrightarrow> indistinguishable A Ms m' m"
proof (induction rule: indistinguishable.induct)
  fix Ms m m'
  show "indistinguishable CommunicationLayer Ms m m'" by rule
  fix A
  show "indistinguishable A Ms m m" by rule
  fix a b c
  assume "\<not> constructible a Ms m'" and "\<not> constructible a Ms m"
  thus "indistinguishable (Actor a) Ms (Signature c m') (Signature b m)" by rule
next
  fix A Ms m m' a
  assume "indistinguishable A Ms m' m"
  thus "indistinguishable A Ms (Signature a m') (Signature a m)" by rule
next
  fix A Ms m m' a b c d
  assume "A \<noteq> Actor c" and "A \<noteq> Actor d" and "A \<noteq> Actor a" and "A \<noteq> Actor b"
  thus "indistinguishable A Ms (Encrypted c d m') (Encrypted a b m)" by rule
next
  fix A Ms m m' a b
  assume "indistinguishable A Ms m' m"
  thus "indistinguishable A Ms (Encrypted a b m') (Encrypted a b m)" by rule
next
  fix A Ms m m' ms ms'
  assume  "indistinguishable A Ms m' m"
    and   "indistinguishable A Ms (MessageList ms') (MessageList ms)"
  thus "indistinguishable A Ms (MessageList (m' # ms')) (MessageList (m # ms))"
    by rule
qed

definition indistinguishability_map ::
  "agent \<Rightarrow> message set \<Rightarrow> (message \<Rightarrow> message) \<Rightarrow> bool" where
    "indistinguishability_map A Ms f
      \<longleftrightarrow> bij f
        \<and> (\<forall> m. indistinguishable A Ms m (f m)
              \<and> (\<forall> a b. (A = Actor a \<and> constructible a Ms m
                          \<longrightarrow> f (Signature b m) = Signature b (f m)
                        )
                      \<and> (A = Actor a \<or> A = Actor b
                          \<longrightarrow> f (Encrypted a b m) = Encrypted a b (f m)
                        )
                )
          )
        \<and> (A \<noteq> CommunicationLayer
            \<longrightarrow> (\<forall> ms. f (MessageList ms) = MessageList (map f ms))
          )"

lemma learnable_indistinguishability_map:
  assumes im: "indistinguishability_map (Actor a) Ms f"
  shows "learnable a M m \<Longrightarrow> learnable a (f M) (f m)"
proof (induction rule: learnable.induct)
  show "learnable a (f M) (f M)" by rule
  fix b m
  assume "learnable a (f M) (f (Encrypted a b m))"
  moreover
    from im have "f (Encrypted a b m) = Encrypted a b (f m)"
      by (unfold indistinguishability_map_def) simp
  ultimately show "learnable a (f M) (f m)" by (metis encrypted1_learnable)
next
  fix b m
  assume "learnable a (f M) (f (Encrypted b a m))"
  moreover
    from im have "f (Encrypted b a m) = Encrypted b a (f m)"
      by (unfold indistinguishability_map_def) simp
  ultimately show "learnable a (f M) (f m)" by (metis encrypted2_learnable)
next
  fix m ms
  assume  minms: "m \<in> set ms"
    and   learnablefms: "learnable a (f M) (f (MessageList ms))"
  from minms have fminfms: "f m \<in> set (map f ms)" by simp
  from im have "f (MessageList ms) = MessageList (map f ms)"
    by (unfold indistinguishability_map_def) simp
  with fminfms and learnablefms show "learnable a (f M) (f m)"
    by (metis listed_learnable)
qed

lemma indistinguishable_message_type:
  "indistinguishable (Actor a) Ms m m' \<Longrightarrow> type_of_message m = type_of_message m'"
proof (induction rule: indistinguishable.cases)
  assume "Actor a = CommunicationLayer"
  thus "type_of_message m = type_of_message m'" ..
next
  fix n
  assume "m = n" and "m' = n"
  thus "type_of_message m = type_of_message m'" by simp
next
  fix b c n n'
  assume "m = Signature b n" and "m' = Signature c n'"
  thus "type_of_message m = type_of_message m'" by simp
  thus "type_of_message m = type_of_message m'" .
next
  fix b c d e n n'
  assume "m = Encrypted b c n" and "m' = Encrypted d e n'"
  thus "type_of_message m = type_of_message m'" by simp
  thus "type_of_message m = type_of_message m'" .
next
  fix n ns n' ns'
  assume "m = MessageList (n # ns)" and "m' = MessageList (n' # ns')"
  thus "type_of_message m = type_of_message m'" by simp
qed

lemma learnable_inverse_indistinguishability_map:
  assumes im: "indistinguishability_map (Actor a) Ms f"
  shows "learnable a M m \<Longrightarrow> learnable a (inv f M) (inv f m)"
proof (induction rule: learnable.induct)
  from im have bij: "bij f" unfolding indistinguishability_map_def ..
  { show "learnable a (inv f M) (inv f M)" by rule
    fix b m
    assume "learnable a (inv f M) (inv f (Encrypted a b m))"
    moreover
      { from im have "f (Encrypted a b (inv f m)) = Encrypted a b (f (inv f m))"
          by (unfold indistinguishability_map_def) simp
        also from bij have "\<dots> = Encrypted a b m" by simp
        finally have "inv f (Encrypted a b m) = Encrypted a b (inv f m)"
          by (simp add: bij)
      }
    ultimately have "learnable a (inv f M) (Encrypted a b (inv f m))" by simp
    thus "learnable a (inv f M) (inv f m)" by rule
  next
    fix b m
    assume "learnable a (inv f M) (inv f (Encrypted b a m))"
    moreover
      { from im have "f (Encrypted b a (inv f m)) = Encrypted b a (f (inv f m))"
          by (unfold indistinguishability_map_def) simp
        also from bij have "\<dots> = Encrypted b a m" by simp
        finally have "inv f (Encrypted b a m) = Encrypted b a (inv f m)"
          by (simp add: bij)
      }
    ultimately have "learnable a (inv f M) (Encrypted b a (inv f m))" by simp
    thus "learnable a (inv f M) (inv f m)" by rule
  next
    fix m ms
    assume  minms: "m \<in> set ms"
      and   learnableinvfms: "learnable a (inv f M) (inv f (MessageList ms))"
    from minms have invfmininvfms: "inv f m \<in> set (map (inv f) ms)" by simp
    from im have
        "f (MessageList (map (inv f) ms)) = MessageList (map f (map (inv f) ms))"
      by (unfold indistinguishability_map_def) simp
    also from bij have "\<dots> = MessageList ms"
      by (subst bij_map_f_map_inv_f) simp_all
    finally have "inv f (MessageList ms) = MessageList (map (inv f) ms)"
      by (simp add: bij)
    with invfmininvfms and learnableinvfms show "learnable a (inv f M) (inv f m)"
      by (metis listed_learnable)
  }
qed

type_synonym encrypted_message_data = "actor \<times> actor \<times> message"
type_synonym
  encrypted_message_map = "encrypted_message_data \<Rightarrow> encrypted_message_data"

subsection {* Events *}
type_synonym payment = nat

datatype_new event
  = Construct actor message   -- {* an actor constructs a message *}
  | Send actor actor message  -- {* send a message from one actor to another *}
  | Receive actor message     -- {* an actor receives a message *}
  | Pay
      actor         -- {* payer *}
      actor         -- {* payee *}
      payment       -- {* specifies amount and manner of payment *}
      nat           -- {* payment id, intended to be unique to payer/payee pair*}

text {* Given a set of past events, which messages has a particular actor received? *}
definition messages_received :: "actor \<Rightarrow> event set \<Rightarrow> message set"
  where "messages_received a es = {m. Receive a m \<in> es}"

text {* What can a particular agent do, given a set of past events and a set of
        concurrent events? *}
inductive doable :: "agent \<Rightarrow> event set \<Rightarrow> event set \<Rightarrow> event \<Rightarrow> bool"
  where communicationLayer_transmit_doable:
          "(Send _ _ m) \<in> es \<Longrightarrow> doable CommunicationLayer es _ (Receive _ m)"
          -- {* the communication layer can cause anyone to receive any message
                previously sent by anyone to anyone *}
      | construct_actor_name: "doable (Actor a) _ _ (Construct a (ActorName _))"
          -- {* an actor can construct any actor's name *}
      | construct_number: "doable (Actor a) _ _ (Construct a (Number _))"
          -- {* an actor can construct any number *}
      | construct_signature:
          "Construct a m \<in> es \<union> es'
            \<Longrightarrow> doable (Actor a) es es' (Construct a (Signature a m))"
          -- {* an actor can construct their own signature for a message they've
                constructed *}
      | construct_encrypted:
          "Construct a m \<in> es \<union> es'
            \<Longrightarrow> doable (Actor a) es es' (Construct a (Encrypted _ _ m))"
          -- {* an actor can construct an encrypted version of a message they've
                constructed *}
      | construct_list:
          "\<forall> m \<in> set ms. Construct a m \<in> es \<union> es'
            \<Longrightarrow> doable (Actor a) es es' (Construct a (MessageList ms))"
          -- {* an actor can construct a list of messages they've constructed *}
      | construct_received:
          "Receive a M \<in> es \<Longrightarrow> learnable a M m
          \<Longrightarrow> doable (Actor a) es _ (Construct a m)"
          -- {* an actor can construct a message they can learn from one they've
                previously received *}
      | constructed_sendable:
          "Construct a m \<in> es \<union> es' \<Longrightarrow> doable (Actor a) es es' (Send a _ m)"
          -- {* an actor can send any message they've constructed *}
      | pay_doable: "doable (Actor a) _ _ (Pay a _ _ _)"
          -- {* an actor can always pay anyone any amount with any payment id *}

text {* Which agent must have caused a particular event? *}
fun causal_agent :: "event \<Rightarrow> agent"
  where construct_cause: "causal_agent (Construct a _) = Actor a"
      | send_cause: "causal_agent (Send a _ _) = Actor a"
      | receive_cause: "causal_agent (Receive _ _) = CommunicationLayer"
      | pay_cause: "causal_agent (Pay a _ _ _) = Actor a"

text {* If an event is doable by an agent, then that agent must be the causal one *}
lemma doable_implies_causal:
  assumes "doable a es es' e"
  shows "causal_agent e = a"
  using assms by (cases rule: doable.cases) simp_all

text {* Which events affect a particular agent? *}
inductive affected_agent :: "agent \<Rightarrow> event \<Rightarrow> bool"
  where constructor_affected: "affected_agent (Actor a) (Construct a _)"
      | sender_affected: "affected_agent (Actor a) (Send a _ _)"
      | send_affects_communication:
          "affected_agent CommunicationLayer (Send _ _ _)"
      | receiver_affected: "affected_agent (Actor a) (Receive a _)"
      | receive_affects_communication:
          "affected_agent CommunicationLayer (Receive _ _)"
      | payer_affected: "affected_agent (Actor a) (Pay a _ _ _)"
      | payee_affected: "affected_agent (Actor a) (Pay _ a _ _)"

text {* The causal agent is always affected *}
lemma causal_affected: "affected_agent (causal_agent e) e"
  by (cases rule: causal_agent.induct) (simp_all add: affected_agent.simps)

fun lift_message_map_event
  :: "(message \<Rightarrow> message) \<Rightarrow> event \<Rightarrow> event"
  where "lift_message_map_event f (Construct a m) = Construct a (f m)"
      | "lift_message_map_event f (Send a b m) = Send a b (f m)"
      | "lift_message_map_event f (Receive a m) = Receive a (f m)"
      | "lift_message_map_event _ e = e"

lemma sendable'_implies_sendable:
  assumes sendable': "Send a b M' \<in> image (lift_message_map_event f) es"
  shows "\<exists> M. f M = M' \<and> Send a b M \<in> es"
proof -
  from sendable' obtain e
    where ein: "e \<in> es" and mape: "lift_message_map_event f e = Send a b M'"
      by auto
  from mape
    obtain M where esend: "e = Send a b M" and mapM: "f M = M'"
      by (cases e) simp_all
  from esend and ein have sendable: "Send a b M \<in> es" by simp
  from mapM and sendable show "\<exists> M. f M = M' \<and> Send a b M \<in> es" by auto
qed

subsection {* Histories *}
type_synonym time = nat
type_synonym history = "(event \<times> time) set"

text {* What events occurred before a given time? *}
definition events_before :: "history \<Rightarrow> time \<Rightarrow> event set" where
  "events_before h t = {e. \<exists> u < t. (e, u) \<in> h}"

lemma events_before_monotonic:
  assumes "t' \<le> t"
  shows "events_before h t' \<subseteq> events_before h t"
proof
  fix e
  assume "e \<in> events_before h t'"
  then obtain t'' where t''t': "t'' < t'" and inh: "(e, t'') \<in> h"
    by (unfold events_before_def) auto
  from t''t' and assms have "t'' < t" by simp
  with inh show "e \<in> events_before h t" by (unfold events_before_def) auto
qed

text {* What events occurred at a given time? *}
definition events_at :: "history \<Rightarrow> time \<Rightarrow> event set" where
  "events_at h t = {e. (e, t) \<in> h}"

abbreviation events_not_after :: "history \<Rightarrow> time \<Rightarrow> event set" where
  "events_not_after h t == events_before h t \<union> events_at h t"

lemma events_not_after: "e \<in> events_not_after h t \<longleftrightarrow> (\<exists> t' \<le> t. (e, t') \<in> h)"
proof
  assume "e \<in> events_not_after h t"
  hence "e \<in> events_before h t \<or> e \<in> events_at h t" by simp
  thus "\<exists> t' \<le> t. (e, t') \<in> h"
  proof
    assume "e \<in> events_before h t"
    then obtain t' where "t' < t" and "(e, t') \<in> h"
      by (unfold events_before_def) auto
    thus "\<exists> t' \<le> t. (e, t') \<in> h" by (simp add: exI[of _ t'])
  next
    assume "e \<in> events_at h t"
    thus "\<exists> t' \<le> t. (e, t') \<in> h" by (unfold events_at_def) (simp add: exI[of _ t])
  qed
next
  assume "\<exists> t' \<le> t. (e, t') \<in> h"
  then obtain t' where t't: "t' \<le> t" and inh: "(e, t') \<in> h" by auto
  show "e \<in> events_not_after h t"
  proof cases
    assume "t' = t"
    with inh show "e \<in> events_not_after h t" by (unfold events_at_def) simp
  next
    assume "t' \<noteq> t"
    with t't have "t' < t" by simp
    with inh show "e \<in> events_not_after h t" by (unfold events_before_def) auto
  qed
qed

lemma events_not_after_monotonic:
  assumes "t' \<le> t"
  shows "events_not_after h t' \<subseteq> events_not_after h t"
proof
  fix e
  assume "e \<in> events_not_after h t'"
  with events_not_after obtain t'' where t''t': "t'' \<le> t'" and inh: "(e, t'') \<in> h"
    by auto
  from t''t' and assms have "t'' \<le> t" by simp
  with inh and events_not_after show "e \<in> events_not_after h t" by auto
qed

text {* Which histories could possibly occur? *}
definition possible_history :: "history \<Rightarrow> bool" where
  "possible_history h \<longleftrightarrow>
    (\<forall> (e, t) \<in> h. doable (causal_agent e) (events_before h t) (events_at h t) e)"

lemma constructed_from_not_received_implies_constructed:
  assumes possible: "possible_history h"
    and   notreceived: "\<not>(\<exists> M'. Receive a M' \<in> events_before h t
                              \<and> constructed_from M' m)"
  shows "constructed_from M m \<Longrightarrow> Construct a M \<in> events_not_after h t
          \<Longrightarrow> Construct a m \<in> events_not_after h t"
proof (induction M)
  fix b
  assume  constructedfrom: "constructed_from (ActorName b) m"
    and   constructed: "Construct a (ActorName b) \<in> events_not_after h t"
  from constructedfrom have "m = ActorName b" by (rule actor_name_constructed_from)
  with constructed show "Construct a m \<in> events_not_after h t" by simp
next
  fix x
  assume  constructedfrom: "constructed_from (Number x) m"
    and   constructed: "Construct a (Number x) \<in> events_not_after h t"
  from constructedfrom have "m = Number x" by (rule number_constructed_from)
  with constructed show "Construct a m \<in> events_not_after h t" by simp
next
  case (Signature b M)
  from Signature.prems(1) have "m = Signature b M \<or> constructed_from M m"
    by (rule signature_constructed_from)
  thus "Construct a m \<in> events_not_after h t"
  proof
    assume "m = Signature b M"
    with Signature.prems(2) show "Construct a m \<in> events_not_after h t" by simp
  next
    assume cfMm: "constructed_from M m"
    let ?casbM = "Construct a (Signature b M)"
    from Signature.prems(2) and events_not_after
      obtain t' where t't: "t' \<le> t" and inh: "(?casbM, t') \<in> h" by auto
    from inh and possible
      have "doable (Actor a) (events_before h t') (events_at h t') ?casbM"
        by (unfold possible_history_def) auto
    hence "Construct a M \<in> events_not_after h t'"
    proof (induction rule: doable.cases)
      case communicationLayer_transmit_doable thus ?case by simp
    next case construct_actor_name thus ?case by simp
    next case construct_number thus ?case by simp
    next
      case construct_signature thus "Construct a M \<in> events_not_after h t'" by simp
    next case construct_encrypted thus ?case by simp
    next case construct_list thus ?case by simp
    next
      case (construct_received a' M' esb sbM esa)
      from `learnable a' M' sbM` have "constructed_from M' sbM"
        by (rule learnable_implies_constructed_from)
      with `?casbM = Construct a' sbM`
        have "constructed_from M' (Signature b M)" by simp
      hence "constructed_from M' M" by rule
      with cfMm have "constructed_from M' m" by (metis constructed_from_transitive)
      moreover
        from construct_received.hyps have "Receive a M' \<in> events_before h t'"
          by simp
        with t't and events_before_monotonic
          have "Receive a M' \<in> events_before h t" by auto
      ultimately
      have "\<exists> M'. Receive a M' \<in> events_before h t \<and> constructed_from M' m"
        by auto
      with notreceived show ?case ..
    next case constructed_sendable thus ?case by simp
    next case pay_doable thus ?case by simp
    qed
    with t't and events_not_after_monotonic
      have "Construct a M \<in> events_not_after h t" by blast
    with cfMm show "Construct a m \<in> events_not_after h t" by (rule Signature.IH)
  qed
next
  case (Encrypted b c M)
  from Encrypted.prems(1) have "m = Encrypted b c M \<or> constructed_from M m"
    by (rule encrypted_constructed_from)
  thus "Construct a m \<in> events_not_after h t"
  proof
    assume "m = Encrypted b c M"
    with Encrypted.prems(2) show "Construct a m \<in> events_not_after h t" by simp
  next
    assume cfMm: "constructed_from M m"
    let ?caebcM = "Construct a (Encrypted b c M)"
    from Encrypted.prems(2) and events_not_after
      obtain t' where t't: "t' \<le> t" and inh: "(?caebcM, t') \<in> h" by auto
    from inh and possible
      have "doable (Actor a) (events_before h t') (events_at h t') ?caebcM"
        by (unfold possible_history_def) auto
    hence "Construct a M \<in> events_not_after h t'"
    proof (induction rule: doable.cases)
      case communicationLayer_transmit_doable thus ?case by simp
    next case construct_actor_name thus ?case by simp
    next case construct_number thus ?case by simp
    next case construct_signature thus ?case by simp
    next
      case construct_encrypted thus "Construct a M \<in> events_not_after h t'" by simp
    next case construct_list thus ?case by simp
    next
      case (construct_received a' M' esb sbM esa)
      from `learnable a' M' sbM` have "constructed_from M' sbM"
        by (rule learnable_implies_constructed_from)
      with `?caebcM = Construct a' sbM`
        have "constructed_from M' (Encrypted b c M)" by simp
      hence "constructed_from M' M" by rule
      with cfMm have "constructed_from M' m" by (metis constructed_from_transitive)
      moreover
        from construct_received.hyps have "Receive a M' \<in> events_before h t'"
          by simp
        with t't and events_before_monotonic
          have "Receive a M' \<in> events_before h t" by auto
      ultimately
      have "\<exists> M'. Receive a M' \<in> events_before h t \<and> constructed_from M' m"
        by auto
      with notreceived show ?case ..
    next case constructed_sendable thus ?case by simp
    next case pay_doable thus ?case by simp
    qed
    with t't and events_not_after_monotonic
      have "Construct a M \<in> events_not_after h t" by blast
    with cfMm show "Construct a m \<in> events_not_after h t" by (rule Encrypted.IH)
  qed
next
  case (MessageList ms)
  from MessageList.prems(1)
    have "m = MessageList ms \<or> (\<exists> M \<in> set ms. constructed_from M m)"
      by (rule list_constructed_from)
  thus "Construct a m \<in> events_not_after h t"
  proof
    assume "m = MessageList ms"
    with MessageList.prems(2) show "Construct a m \<in> events_not_after h t"
      by simp
  next
    assume "\<exists> M \<in> set ms. constructed_from M m"
    then obtain M
        where Minms: "M \<in> set ms"
          and cfMm: "constructed_from M m"
      by auto
    let ?camms = "Construct a (MessageList ms)"
    from MessageList.prems(2) and events_not_after
      obtain t' where t't: "t' \<le> t" and inh: "(?camms, t') \<in> h" by auto
    from inh and possible
      have "doable (Actor a) (events_before h t') (events_at h t') ?camms"
        by (unfold possible_history_def) auto
    hence "Construct a M \<in> events_not_after h t'"
    proof (induction rule: doable.cases)
      case communicationLayer_transmit_doable thus ?case by simp
    next case construct_actor_name thus ?case by simp
    next case construct_number thus ?case by simp
    next case construct_signature thus ?case by simp
    next case construct_encrypted thus ?case by simp
    next
      case construct_list
      with Minms show "Construct a M \<in> events_not_after h t'" by simp
    next
      case (construct_received a' M' esb sbM esa)
      from `learnable a' M' sbM` have "constructed_from M' sbM"
        by (rule learnable_implies_constructed_from)
      with `?camms = Construct a' sbM`
        have "constructed_from M' (MessageList ms)" by simp
      with Minms have "constructed_from M' M" by rule
      with cfMm have "constructed_from M' m" by (metis constructed_from_transitive)
      moreover
        from construct_received.hyps have "Receive a M' \<in> events_before h t'"
          by simp
        with t't and events_before_monotonic
          have "Receive a M' \<in> events_before h t" by auto
      ultimately
      have "\<exists> M'. Receive a M' \<in> events_before h t \<and> constructed_from M' m"
        by auto
      with notreceived show ?case ..
    next case constructed_sendable thus ?case by simp
    next case pay_doable thus ?case by simp
    qed
    with t't and events_not_after_monotonic
      have "Construct a M \<in> events_not_after h t" by blast
    with Minms and cfMm show "Construct a m \<in> events_not_after h t"
      by (rule MessageList.IH)
  qed
qed

text {* What messages did a given agent receive before a given time? *}
fun messages_received_before :: "agent \<Rightarrow> history \<Rightarrow> time \<Rightarrow> message set"
  where "messages_received_before CommunicationLayer _ _ = {}"
      | "messages_received_before (Actor a) h t
          = {m. Receive a m \<in> events_before h t}"

text {* What subhistory is relevant to a given agent before a given time? *}
definition relevant_subhistory :: "agent \<Rightarrow> time \<Rightarrow> history \<Rightarrow> history" where
  "relevant_subhistory A t h = {(e, t') \<in> h. affected_agent A e \<and> t' < t}"

end
