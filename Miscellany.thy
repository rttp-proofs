header {* Miscellaneous lemmas *}
theory Miscellany
imports Main
begin

lemma bij_f_inv_f [simp]:
  assumes "bij f"
  shows "f (inv f x) = x"
  by (metis UNIV_I assms bij_betw_imp_surj f_inv_into_f)

lemma bij_f_x_equiv_inv_f_y [simp]:
  assumes "bij f"
  shows "inv f y = x \<longleftrightarrow> f x = y"
  by (metis assms bij_betw_def bij_f_inv_f inv_f_eq)

lemma bij_map_f_map_inv_f [simp]:
  assumes "bij f"
  shows "map f (map (inv f) xs) = xs"
  by (metis assms bij_betw_imp_surj list.map_id map_map surj_iff)

end
